/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * This is a part of the inputmethod-common static Java library.
 * The original source code can be found at frameworks/opt/inputmethodcommon of Android Open Source
 * Project.
 */

package com.android.inputmethodcommon;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.graphics.drawable.Drawable;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceScreen;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.inputmethod.InputMethodInfo;
import android.view.inputmethod.InputMethodManager;
import android.view.inputmethod.InputMethodSubtype;

import java.util.List;

class InputMethodSettings {
    private Context mContext;
    private InputMethodManager mManager;
    private InputMethodInfo mMethodInfo;

    private Preference mSubtypeEnablerPreference;

    private int mSubtypeEnablerTitleRes;
    private CharSequence mSubtypeEnablerTitle;
    private int mSubtypeEnablerIconRes;
    private Drawable mSubtypeEnablerIcon;

    /**
     * Initialize internal states of this object.
	 *
     * @param context the context for this application.
     * @param prefScreen a PreferenceScreen of PreferenceActivity or PreferenceFragment.
     * @return true if this application is an IME and has two or more subtypes, false otherwise.
     */
    public boolean init(final Context context, final PreferenceScreen prefScreen) {
        mContext = context;
        mManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        mMethodInfo = getInputMethodInfo(context, mManager);
        if (mMethodInfo == null || mMethodInfo.getSubtypeCount() <= 1) return false;

        mSubtypeEnablerPreference = new Preference(context);
        mSubtypeEnablerPreference.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			@Override
			public boolean onPreferenceClick(Preference preference) {
				CharSequence title = getSubtypeEnablerTitle(context);
				Intent intent = new Intent(Settings.ACTION_INPUT_METHOD_SUBTYPE_SETTINGS);
				intent.putExtra(Settings.EXTRA_INPUT_METHOD_ID, mMethodInfo.getId());
				if (!TextUtils.isEmpty(title)) {
					intent.putExtra(Intent.EXTRA_TITLE, title);
				}
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
						| Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED
						| Intent.FLAG_ACTIVITY_CLEAR_TOP);
				context.startActivity(intent);
				return true;
			}
		});
        prefScreen.addPreference(mSubtypeEnablerPreference);
        updateSubtypeEnabler();
		return true;
    }

    private static InputMethodInfo getInputMethodInfo(Context context, InputMethodManager manager) {
        final List<InputMethodInfo> infosList = manager.getInputMethodList();
        for (InputMethodInfo info : infosList) {
			if (info.getPackageName().equals(context.getPackageName())) return info;
		}
        return null;
    }

    private static String getEnabledSubtypesLabel(Context context, InputMethodManager manager, InputMethodInfo info) {
        if (context == null || manager == null || info == null) return null;

		ApplicationInfo appInfo = info.getServiceInfo().applicationInfo;
        List<InputMethodSubtype> subtypesList = manager.getEnabledInputMethodSubtypeList(info, true);
        StringBuilder builder = new StringBuilder();
		for (InputMethodSubtype subtype : subtypesList) {
            if (builder.length() > 0) builder.append(", ");
            builder.append(subtype.getDisplayName(context, info.getPackageName(), appInfo));
        }
        return builder.toString();
    }

    public void setSubtypeEnablerTitle(int resId) {
        mSubtypeEnablerTitleRes = resId;
        updateSubtypeEnabler();
    }

    public void setSubtypeEnablerTitle(CharSequence title) {
        mSubtypeEnablerTitleRes = 0;
        mSubtypeEnablerTitle = title;
        updateSubtypeEnabler();
    }

    public void setSubtypeEnablerIcon(int resId) {
        mSubtypeEnablerIconRes = resId;
        updateSubtypeEnabler();
    }

    public void setSubtypeEnablerIcon(Drawable drawable) {
        mSubtypeEnablerIconRes = 0;
        mSubtypeEnablerIcon = drawable;
        updateSubtypeEnabler();
    }

    private CharSequence getSubtypeEnablerTitle(Context context) {
        if (mSubtypeEnablerTitleRes != 0) {
            return context.getString(mSubtypeEnablerTitleRes);
        } else {
            return mSubtypeEnablerTitle;
        }
    }

    public void updateSubtypeEnabler() {
        if (mSubtypeEnablerPreference == null) return;

		if (mSubtypeEnablerTitleRes != 0) {
			mSubtypeEnablerPreference.setTitle(mSubtypeEnablerTitleRes);
		} else if (!TextUtils.isEmpty(mSubtypeEnablerTitle)) {
			mSubtypeEnablerPreference.setTitle(mSubtypeEnablerTitle);
		}

		String summary = getEnabledSubtypesLabel(mContext, mManager, mMethodInfo);
		if (!TextUtils.isEmpty(summary)) {
			mSubtypeEnablerPreference.setSummary(summary);
		}

		if (mSubtypeEnablerIconRes != 0) {
			mSubtypeEnablerPreference.setIcon(mSubtypeEnablerIconRes);
		} else if (mSubtypeEnablerIcon != null) {
			mSubtypeEnablerPreference.setIcon(mSubtypeEnablerIcon);
		}
    }
}
